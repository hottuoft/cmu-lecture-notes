#include<stdio.h>

#define NOTHING_MODE 0
#define INLINE_MODE 1
#define INLINE_DISPLAY_MODE 2
#define DISPLAY_MODE 3

int main() {
  int rd;
  int mode = 0;
  while((rd = getchar()) != EOF) {
    if(rd != '$') putchar(rd);
    else {
      int nxt = getchar();
      if(nxt == '$') {
        switch(mode) {
          case NOTHING_MODE:
            putchar('\\');
            putchar('[');
	    mode = DISPLAY_MODE;
            break;
          case INLINE_DISPLAY_MODE:
          case INLINE_MODE:
            putchar('\\');
            putchar(')');
            ungetc('$', stdin);
	    if(mode == INLINE_MODE) { mode = NOTHING_MODE; }
	    else { mode = DISPLAY_MODE; }  
            break;
          case  DISPLAY_MODE:
            putchar('\\');
            putchar(']');
	    mode = NOTHING_MODE;
            break;
        }
      }
      else if(mode == INLINE_MODE || mode == INLINE_DISPLAY_MODE) {
        putchar('\\'); putchar(')');
        if(mode == INLINE_MODE) {
          mode = NOTHING_MODE;
        } else {
          mode = DISPLAY_MODE;
        }
      }
      else {
        putchar('\\'); putchar('(');
        if(mode == DISPLAY_MODE) {
          mode = INLINE_DISPLAY_MODE;
        } else {
          mode = INLINE_MODE;
        }
      }

      if(nxt == EOF) break; else if(nxt != '$') putchar(nxt);
    }
  }
}
